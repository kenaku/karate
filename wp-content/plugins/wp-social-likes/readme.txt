=== Social Likes ===
Contributors: tssoft
Tags: facebook, twitter, vk.com, vkontakte, google+, pinterest, livejournal, odnoklassniki, mail.ru, social links, share buttons, social, social buttons, jquery
Requires at least: 3.0
Tested Up To: 3.5.1
Stable tag: trunk
License: MIT
License URI: https://raw.github.com/tssoft/wp-social-likes/master/license.md

Single style buttons with like counters: Facebook, Twitter, Google+, Pinterest, LiveJournal and also popular Russian social networks.

== Description ==

WordPress plugin for Social Likes library by Artem Sapegin (http://sapegin.me/projects/social-likes)

== Screenshots ==

1. Settings page allows you to customize list of website buttons and how they appear on page
2. Option in editor allows to control appereance of buttons for every post or page 

== Changelog ==

= 1.5 =
 * Fixed bug with appearance of websites on settings page for English version of WordPress

= 1.4 =
 * Option to place first image in the post/page to the Image URL field (required by Pinterest)
 * Russian social network buttons now available for English version of WordPress

= 1.3 =
 * Default message text based on the title of page/post being shared (Feedback from Yevgen Timashov)

= 1.2 =
 * Smarter appearance of buttons: if post doesn't contain "more" tag, they will appear both on single post page and on page with multiple posts
 * English and Russian tooltips for share buttons depending on language of current page/post

= 1.1 =
 * Commands for adding buttons to existing posts and/or pages

= 1.0 =
 * First release