<?php
/*
 * Template Name: Киокушин каратэ
 */

get_header(); ?>
<?php
  $children = wp_list_pages("title_li=&child_of="."26"."&echo=0");
?>
</div> <!-- row -->
<div class="row">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  <section class="col8">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title col12"><?php the_title(); ?></h1>
			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
			</div><!-- .entry-content -->
			</article><!-- #post-## -->
  </section>
  <aside class="col4 last">
    <ul class="kiokushin-menu">
      <?php echo $children; ?>
    </ul>
    <?php
    $aforizm = get_post_meta(get_the_ID(), 'афоризм',   true);
    $aforizm_autor = get_post_meta(get_the_ID(), 'автор афоризма',   true);
    if( ! empty( $aforizm ) ){ ?>
    <div class="aforizm">
      <p><?php echo $aforizm ?></p>
      <p class="aforizm-autor"><?php echo $aforizm_autor ?></p>
    </div>
    <?php }; ?>
  </aside>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>
