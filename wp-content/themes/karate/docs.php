<?php
/*
 * Template Name: Документы
 */
get_header(); ?>
<?php
  $id = get_the_id();
  ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h1 class="entry-title">Документы</h1>
        <ul class="docs-nav clearfix">
      <?php if ($id == 444){ ?>
        <li class="docs-nav_active">Учредительные</li>
        <li><a href="<?php echo home_url( '/' ); ?>/reglamentiruyushhie">Регламентирующие</a></li>
      <?php } else{ ?>
        <li><a href="<?php echo home_url( '/' ); ?>/dokumenty">Учредительные</a></li>
        <li class="docs-nav_active">Регламентирующие</li>
      <?php } ?>
        </ul>
          <div class="entry-content">
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
            <?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
          </div><!-- .entry-content -->
        </article><!-- #post-## -->
<?php endwhile; ?>
<?php get_footer(); ?>