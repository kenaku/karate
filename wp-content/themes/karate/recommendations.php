<?php
/*
 * Template Name: Рекомендации
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title col12"><?php the_title(); ?></h1>
			<!-- <div class="col7 entry-content"><?php the_content(); ?></div> -->
<?php endwhile; ?>
    <div class="col9">
    <?php
      $categories = get_categories('child_of=11');
      foreach ($categories as $category) {
    ?>
      <h3 class="recommend-cat_title"><?php echo $category->name ?></h3>
      <?php
        $args = array( 'post_type' => 'recommendation', 'cat' =>'$category->cat_ID' ,'posts_per_page' => 7 );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
          $catid = get_the_category();
        if ($catid[0]->term_id == $category->cat_ID):
       ?>
      <h4 class="recommend-entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
      <?php the_excerpt(); ?>

      <?php endif; endwhile; };
      ?>
    </div>
</div>
<?php get_footer(); ?>
