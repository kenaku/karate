<?php
/*
 * Template Name: Спорт
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <h1 class="entry-title col12"><?php the_title(); ?></h1>
    <p class="col5 rukov_description"><?php bloginfo( 'description' ); ?></p>
</div> <!-- row -->

<!-- Попечительский совет -->
<div class="row">
  <h2 class="col12 rukov_subheader">Попечительский совет</h2>
  <?php
    $pop_sovet = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order' ) ); // Берем дочерние страницы в цикл
    foreach( $pop_sovet as $pop_page ) {
    if (!($pop_page->ID == 318 )){ // Исключаем из списка страницу президента
  ?>
  <div class="rukov-pop-sovet col2 col4s">
    <a class="modalLink" href="#modal<?php echo $pop_page->ID ?>"><?php echo get_the_post_thumbnail($pop_page->ID, 'full'); ?></a>
    <?php echo $pop_page->post_title; ?>
  </div>
  <div id="modal<?php echo $pop_page->ID ?>" class="modal">
      <div class="closeBtn"></div>
      <h2><?php echo $pop_page->post_title; ?></h2>
      <div class="alignleft modal_thumb"><?php echo get_the_post_thumbnail($pop_page->ID, 'full'); ?></div>
      <div class="modal_text"><?php echo $pop_page->post_content; ?></div>
  </div>
  <?php  }} ?>
  <div class="overlay"></div>
</div> <!-- row -->
<!-- Попечительский совет всё-->

<!-- Президент -->
<div class="row">
  <?php $page_id = 318;
    $president = get_page( $page_id );
    $content = $president->post_content;
    if ( ! $content ) // Check for empty page
    continue;
    $content = apply_filters( 'the_content', $content );
  ?>
  <h2 class="col12 rukov_subheader">Президент</h2>
  <div class="col3 last president-thumb"><a class="modalLink" href="#modal<?php echo $president->ID ?>"><?php echo get_the_post_thumbnail($president->ID, array(188,240)); ?></a></div>
  <div class="col9 last rukov_president-excerpt">
    <?php
      echo '<p class="rukov_president-name">' . $president->post_title . '</p>';
      echo $president->post_excerpt;
    ?>
  </div>
  <div id="modal<?php echo $president->ID ?>" class="modal">
    <div class="closeBtn"></div>
    <h2><?php echo $president->post_title; ?></h2>
    <div class="alignleft modal_thumb"><?php echo get_the_post_thumbnail($president->ID, array(188,240)); ?></div>
    <div class="modal_text"><?php echo $content; ?></div>
  </div>
</div>
<!-- Президент всё-->

<!-- Содержимое страницы -->
<div class="row">
  <div class="entry-content  col12">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <h2 class="col12 rukov_subheader">Исполнительный комитет</h2>
      <?php the_content(); ?>
    </article><!-- #post-## -->
  </div><!-- entry-content -->
</div>
<?php endwhile; ?>
<!-- Содержимое страницы всё -->

<?php get_footer(); ?>
