<?php
/*
 * Template Name: Контакты
 */

  get_header(); ?>
  </div> <!-- row -->
</div> <!-- container -->
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="contacts-container">
  <div class="container contacts-content">
    <div class="row contacts-row">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h1 class="entry-title col12"><?php the_title(); ?></h1>
        <div class="col6 contacts-content_block"><?php the_content(); ?></div>
      </article><!-- #post-## -->
    </div>
  </div>
  <div class="map-container">
    <div class="responsive-embed-container">
      <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=1lJCbfgBpFab4VKzisX3QtIN67kLjNQ8&width=600&height=450"></script>
    </div>
  </div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>
