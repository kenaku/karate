// Настройки слайдера для шапки на главной
$(document).ready(function() {

  $("#owl-example").owlCarousel({
    navigation : true,
    pagination : false,
    items : 4,
    navigationText:false,
  });

});
      $(function () {

        var $box = $('.header-slider-box')
          , $indicators = $('.goto-slide')
          , $effects = $('.effect')
          , $timeIndicator = $('#time-indicator')
          , slideInterval = 5000
          , effectOptions = {
              'blindLeft': {blindCount: 15}
            , 'blindDown': {blindCount: 15}
            , 'tile3d': {tileRows: 6, rowOffset: 80}
            , 'tile': {tileRows: 6, rowOffset: 80}
          };

        // This function runs before the slide transition starts
        var switchIndicator = function ($c, $n, currIndex, nextIndex) {
          // kills the timeline by setting it's width to zero
          $timeIndicator.stop().css('width', 0);
          // Highlights the next slide pagination control
          $indicators.removeClass('current').eq(nextIndex).addClass('current');
        };

        // This function runs after the slide transition finishes
        var startTimeIndicator = function () {
          // start the timeline animation
          $timeIndicator.animate({width: '680px'}, slideInterval);
        };

        // initialize the plugin with the desired settings
        $box.boxSlider({
            speed: 1000
          , autoScroll: true
          , timeout: slideInterval
          , next: '#next'
          , prev: '#prev'
          , pause: '#pause'
          , effect: 'fade'
          , onbefore: switchIndicator
          , onafter: startTimeIndicator
        });

        startTimeIndicator(); // start the time line for the first slide

        // Paginate the slides using the indicator controls
        $('#controls').on('click', '.goto-slide', function (ev) {
          $box.boxSlider('showSlide', $(this).data('slideindex'));
          ev.preventDefault();
        });

        // This is for demo purposes only, kills the plugin and resets it with
        // the newly selected effect FIXME clean this up!
        $('#effect-list').on('click', '.effect', function (ev) {
          var $effect = $(this)
            , fx = $effect.data('fx')
            , extraOptions = effectOptions[fx];

          $effects.removeClass('current');
          $effect.addClass('current');
          switchIndicator(null, null, 0, 0);

          if (extraOptions) {
            $.each(extraOptions, function (opt, val) {
              $box.boxSlider('option', opt, val);
            });
          }

          $box.boxSlider('option', 'effect', $effect.data('fx'));

          ev.preventDefault();
        });

      });

// Настройки модальных окон с затенением фона
 $(document).ready(function(){
    $('.modalLink').modal({
        trigger: '.modalLink',      // id or class of link or button to trigger modal
        olay:'div.overlay',         // id or class of overlay
        modals:'div.modal',         // id or class of modal
        animationEffect: 'fadeIn',  // overlay effect | slideDown or fadeIn | default=fadeIn
        animationSpeed: 200,        // speed of overlay in milliseconds | default=400
        moveModalSpeed: 'slow',     // speed of modal movement when window is resized | slow or fast | default=false
        background: '000',          // hexidecimal color code - DONT USE #
        opacity: 0.6,               // opacity of modal |  0 - 1 | default = 0.8
        openOnLoad: false,          // open modal on page load | true or false | default=false
        docClose: true,             // click document to close | true or false | default=true
        closeByEscape: true,        // close modal by escape key | true or false | default=true
        moveOnScroll: false,         // move modal when window is scrolled | true or false | default=false
        resizeWindow: true,         // move modal when window is resized | true or false | default=false
        close:'.closeBtn'           // id or class of close button
    });
  });

// функция закрывает окна программ карате на главной
 $(".program-click, .program-close-btn").click(function () {
    $(".tab-pane").removeClass("active");
  });