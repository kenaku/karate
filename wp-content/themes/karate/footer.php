<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>
</section><!-- #main -->
<footer role="contentinfo" class="container site-footer">
   <div class="row">
     <div class="footer-logo col1 col3s"><img src="<?php bloginfo( 'template_directory' ); ?>/images/footer-logo.png" alt="<?php bloginfo( 'description' ); ?>"></div>
	    <div class="col5 footer-copy">&copy; <?php print date(Y); ?>, <?php bloginfo( 'description' ); ?></div>
       <?php wp_nav_menu( array('menu' => 'footer-nav', 'container' => 'nav','container_class' => 'col6 last footer-links', )); ?>
   </div>
</footer><!-- footer -->

<!-- Подключаемые js библиотеки -->
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jquery1-10-1.min.js"></script>
<!-- // <script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/cssrefresh.js"></script> -->
<script src="<?php bloginfo( 'template_directory' ); ?>/js/box-slider-fade.jquery.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/js/owl.carousel.min.js"></script>
<script src='<?php bloginfo( 'template_directory' ); ?>/js/jquery.modal.min.js'></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/bootstrap-tab.js"></script>
<!-- Самописные функции и настройки разных js плагинов и библиотек -->
<script src='<?php bloginfo( 'template_directory' ); ?>/js/functions.js'></script>
<?php	wp_footer(); ?>
</body>
</html>