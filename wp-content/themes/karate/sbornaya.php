<?php
/*
 * Template Name: Сборная
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title col12"><?php the_title(); ?></h1>
			<div class="col6 entry-content"><?php the_content(); ?></div>
        <aside>
          <?php
            $rating_link  = get_post_meta(get_the_ID(), 'рейтинг_ссылка',   true);
            $rating_title = get_post_meta(get_the_ID(), 'рейтинг_название', true);
            $list_link    = get_post_meta(get_the_ID(), 'список_ссылка',    true);
            $list_title   = get_post_meta(get_the_ID(), 'список_название',  true);
          ?>
          <div class="col3 last">
            <a href="<?php if( ! empty( $rating_link ) ) echo "$rating_link"; ?>" class="sbornaya_doc">
              <div class="col2 col3s">
                <img src="<?php bloginfo( 'template_directory' ); ?>/images/doc2.png"  alt="Рейтинг сборной ОФСО  СККР">
              </div>
              <div class="col8 last">
                <?php if( ! empty( $rating_title ) ) echo "$rating_title"; ?>
                <p class="sbornaya_list-p">(124кб, doc)</p>
              </div>
            </a>
          </div>
          <div class="col3 last">
            <a href="<?php if( ! empty( $list_link ) ) echo "$list_link"; ?>" class="sbornaya_doc">
              <div class="col2 col3s">
                <img src="<?php bloginfo( 'template_directory' ); ?>/images/doc.png"  alt="Список сборной ОФСО  СККР">
              </div>
              <div class="col8 last">
                <?php if( ! empty( $list_title ) ) echo "$list_title"; ?>
                <p class="sbornaya_list-p">(249кб, doc)</p>
              </div>
            </a>
          </div>
        </aside>
  </article><!-- #post-## -->
</div> <!-- row -->
    <div class="row sportsmans">
      <?php $sportsmans = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'menu_order' ) ); // Берем дочерние страницы в цикл
        $i = 1;
        foreach( $sportsmans as $sportsman ) {
          $dan = get_post_meta($sportsman->ID, 'дан', true);
          $rank = get_post_meta($sportsman->ID, 'звание', true);
      ?>
      <div class="col3 col4s sportsman-block <?php if($i % 4 === 0) echo 'last' ?>">
        <a class="modalLink" href="#modal<?php echo $sportsman->ID ?>">
          <?php if ( '' != get_the_post_thumbnail($sportsman->ID) ) {
            echo get_the_post_thumbnail($sportsman->ID, 'full');
            }else{?>
              <div class="spartsman-no-photo">Фото временно отсутствует</div>
            <?php }; ?>
        </a>
        <div class="sportsman_description">
          <?php echo $sportsman->post_title; ?>
          <p class="sportsman_rank"><?php echo $rank ?></p>
          <!-- <p class="sportsman_dan"><?php if( ! empty( $dan ) ) echo $dan . ' дан' ?></p> -->
        </div>
      </div>
    <div id="modal<?php echo $sportsman->ID ?>" class="modal">
      <div class="closeBtn"></div>
      <!-- <h2><?php echo $sportsman->post_title; ?></h2> -->
      <!-- <div class="alignleft modal_thumb"><?php echo get_the_post_thumbnail($sportsman->ID, array(188,240)); ?></div> -->
      <div class="modal_text"><?php echo $sportsman->post_content; ?></div>
    </div>
    <?php $i++; } ?>
    </div>
<?php endwhile; ?>
<div class="overlay"></div>
</div>
<?php get_footer(); ?>