<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
      <div class="col12">
        <h1 class="post-header">
          <?php the_title(); ?> <span class="entry-utility"><?php the_date() ?></span>
        </h1>
      </div> <!-- col12 -->
    </div> <!-- row -->
  </div> <!-- container -->
  <div class="container entry-excerpt">
    <div class="row">
      <div class="col12">
        <div class="entry-excerpt_text">
          <?php the_excerpt(); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="container entry-content_container">
      <div class="row">
        <div class="col12">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

          <div class="entry-content">
            <?php if ( has_post_thumbnail() ) the_post_thumbnail(array(480,360));?>
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
          </div><!-- .entry-content -->

        </article><!-- #post-## -->
        <nav id="nav-below clearfix" class="navigation">
          <div class="nav-previous alignleft"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'boilerplate' ) . '</span> %title' ); ?>&nbsp&nbsp&nbsp</div>
          <div class="nav-next alignright"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'boilerplate' ) . '</span>' ); ?></div>
        </nav><!-- #nav-below -->
      </div>
        <?php comments_template( '', true ); ?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
